function [ripple]= mcz_rippledayprocess(rawDir, dataDir, animID, sessionNum, varargin)
% This function creates ripple filtered EEGs from local referenced EEG files.

%rawDir -- the directory where the raw dat folders are located
%dataDir -- the directory where the processed files should be saved
%animID -- a string identifying the animal's id (appended to the
%beginning of the files)
%sessionNum -- the session number (in chronological order for the animal)
%
% Varargin Options
%-----------------
%		'f', matfilename
%			specifies the name of the mat file containing the
%			ripple filter to use
%			(default filters/ripplefilter.mat).
%			Note that the filter must be called 'ripplefilter'.
%		'ref', 0 or 1
%			specifies whether to use eegref (1) or eeg (0)
% TO DO: Add ability to only run rippledayprocess on specific tetrodes,
% currently runs through all tetrodes and epochs on a given day
% RN EDIT 8/8/17: Skips filtering if file already exists
% RN EDIT 11/7/17: Accepts varargin 'ref' can be set to 1 or 0. If 1 uses eegref and savefile is gammaref. If 0 uses eeg and save is gamma.

% Default for ripple filtering is to apply filter to eegref
eegStr = 'eegref';

% Default ripple filter
f = [fileparts(mfilename('fullpath')) filesep 'filters/ripplefilter.mat'];

%set variable options
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'f'
	    f = varargin{option+1};
        case 'ref'
	    if varargin{option+1}==1
		eegStr = 'eegref';
	    elseif varargin{option+1}==0
		eegStr = 'eeg';
	    else
		error('Invalid ref option. Must be 0 or 1')
	    end
    end
end

if isempty(f)
    error('Filter not found!');
else
    eval(['load ', f]);
end
currDir = pwd;
cd(dataDir);

if isempty(dir(['*EEG']));
    error('No EEG folder found!');
end

cd('EEG');
sString= sprintf('%02i',sessionNum);

relFiles= dir([animID eegStr sString '*.mat']);
for i=1:length(relFiles);
    x = sscanf(relFiles(i).name,[animID eegStr '%d-%d-%d.mat']);
    e = x(2);
    t = x(3);
    saveFile = strrep(relFiles(i).name,'eeg','ripple');
    if ~isempty(dir(saveFile))
        fprintf('Ripple filtered file already exists for day %02d. Skipping...\n',sessionNum);
        continue;
    else
        fprintf('Ripple Filtering LFP for %02i-%02i-%02i\n',sessionNum,e,t);
    end

    eegref=load(relFiles(i).name);
    eegref = eegref.(eegStr); % so actually eegref var will be eegref or eeg depending on ref option given

    ripple{sessionNum}{e}{t} = filtereeg2(eegref{sessionNum}{e}{t}, ripplefilter, 'int16', 1);
    ripple{sessionNum}{e}{t}.voltage_scaling    =eegref{sessionNum}{e}{t}.voltage_scaling;
    ripple{sessionNum}{e}{t}.low_pass_filter    =eegref{sessionNum}{e}{t}.low_pass_filter;
    ripple{sessionNum}{e}{t}.referenced         =eegref{sessionNum}{e}{t}.referenced;
    ripple{sessionNum}{e}{t}.data_voltage_scaled=eegref{sessionNum}{e}{t}.data_voltage_scaled;
    ripple{sessionNum}{e}{t}.nTrodeChannel      =eegref{sessionNum}{e}{t}.nTrodeChannel;
    ripple{sessionNum}{e}{t}.nTrode             =eegref{sessionNum}{e}{t}.nTrode;
    ripple{sessionNum}{e}{t}.endtime            =eegref{sessionNum}{e}{t}.endtime;
    ripple{sessionNum}{e}{t}.clockrate          =eegref{sessionNum}{e}{t}.clockrate;
    ripple{sessionNum}{e}{t}.timerange          =eegref{sessionNum}{e}{t}.timerange;

    % save the resulting file
    if strcmp(eegStr,'eegref')
	rippleref=ripple;
	save(saveFile,'rippleref');
    else
        save(saveFile,'ripple');
    end
    clear rippleref ripple eegref

end
cd(currDir)
