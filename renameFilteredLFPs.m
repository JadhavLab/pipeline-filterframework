function out = renameFilteredLFPs(topDir)
% renameFilteredLFPs(topDir) will recursively progress through topDir looking for
% delta, gamma, ripple and theta files and renaming the files and the contained
% varialbes to _____ref if the LFP that was filtered was previously referenced.
% So RZ1gamma01-01-02.mat will become RZ1gammaref01-01-02.mat if a referenced
% LFP was filtered to make the gamma files. pwd is used if no topDir given.
% THIS WILL ONLY WORK WHEN THE ANIMAL ID IS ALL CAPS OR ENDS IN A NUMBER.
out =0;
if ~exist('topDir','var')
    topDir = pwd;
end
if topDir(end)==filesep
    topDir = topDir(1:end-1);
end

allDir = dir(topDir);
files = [];

% check is topDir is EEG folder
[dirPath,dirName] = fileparts(topDir);
if strcmp(dirName,'EEG')
    % process LFP files
    eegDir = dir([topDir filesep '*.mat']);
    expression = '(?<animID>[A-Z0-9]*)(?<type>[a-z]*)(?<day>\d{2})-(?<epoch>\d{2})-(?<tet>\d{2}).mat';
    for i=1:numel(eegDir),
        if eegDir(i).name(1)=='.'
            continue;
        end
        fileDeets = regexp(eegDir(i).name,expression,'names');
        if isempty(fileDeets)
            fprintf('Could not process %s. Continuing...\n',[topDir filesep eegDir(i).name]);
            continue;
        end
        day = str2double(fileDeets.day);
        epoch = str2double(fileDeets.epoch);
        tet = str2double(fileDeets.tet);
        tag = fileDeets.type;

        % ref check
        dat = load([topDir filesep eegDir(i).name]);
        refCheck = dat.(tag){day}{epoch}{tet}.referenced;
        
        % tag check
        tagCheck = strcmp(tag(end-2:end),'ref');
        
        if tagCheck && refCheck
            continue;
        elseif tagCheck && ~refCheck
            newTag = tag(1:end-3);
        elseif ~tagCheck && refCheck
            newTag = [tag 'ref'];
        else
            continue;
        end
        % Fix tag and enclosed variable
         eval([newTag '= dat.(tag);'])
         newFile = [topDir filesep strrep(eegDir(i).name,tag,newTag)];
         fprintf('Converting %s --> %s\n',eegDir(i).name,strrep(eegDir(i).name,tag,newTag));
         save(newFile,newTag)
         delete([topDir filesep eegDir(i).name]) 
     end
     out =1;
else
    out = 1;
    for i=1:numel(allDir),
        if allDir(i).name(1)=='.'
            continue;
        end
        if allDir(i).isdir
            out1 = renameFilteredLFPs([topDir filesep allDir(i).name]);
            if ~out1
                out = 0;
            end
        end
    end
end

        
