function rn_createNQPosFiles(rawDir, dataDir, animID, sessionNum, dioFramePulseChannelName, varargin)
% createNQPosFiles_new(rawDir, dataDir,animID,sessionNum, dioFramePulseChannelName)
%
% This function extracts POS information and saves data in the FF format.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assumes position tracking WITH range and cm per pix already calculated in the pipeline.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The function also assumes that there is a '*.time' folder in the current directory
% conaining time information about the session (from
% extractTimeBinaryFile.m).
%
%
% rawDir -- the directory where the raw dat folders are located
% dataDir -- the directory where the processed files should be saved
% animID -- a string identifying the animal's id (appended to the
% beginning of the files).
% sessionNum -- the session number (in chronological order for the animal)
% dioFramePulseChannelName -- dio channel used for pulsed frames (mcz added 12/09/16)
% Adapted from mcz_createNQPosFiles_new and createNQPosFiles
% Fixed by Roshan 4/8/2017


if ~exist('dioPulseFrameChannel','var')
    dioFramePulseChannelName=[]; % default strobing channel not used
end

posfilt = gaussian(30*0.5, 60); % gaussian smoothing for velocity filter
diodenum = 2;  % number of diodes/indicators used
diode = .5; % use average between front and back diodes
reversex = 0; % don't flip pos along x
reversey = 0; % don't flip pos along y
maxv= 300; % maximum velocity in cm/s
maxdevpoints= 30;
bounds= [0 0 ; 1000 1000]; % bounds of video
pinterp = 1; % do interpolate
maxpinterp = 1e100;     % allow interpolation of any number of points
maxdinterp = 1e100;     % allow interpolation over any distance

for option = 1:2:length(varargin)-1
    if ischar(varargin{option})
        switch(varargin{option})
            case 'posfilt'
                posfilt = varargin{option+1};
            case 'diodepos'
                diodepos = varargin{option+1};
            case 'diodenum'
                diodenum = varargin{option+1};
            case 'reversex'
                reversex = varargin{option+1};
            case 'reversey'
                reversey = varargin{option+1};
            case 'maxv'
                maxv = varargin{option+1};
            case 'maxdevpoints'
                maxdevpoints = varargin{option+1};
            case 'bounds'
                bounds = varargin{option+1};
            case 'pinterp'
                pinterp = varargin{option+1};
            case 'maxpinterp'
                maxpinterp = varargin{option+1};
            case 'maxdinterp'
                maxdinterp = varargin{option+1};
            case 'dioFramePulseChannelName'
                dioFramePulseChannelName = varargin{option+1};
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end
    else
        error('Options must be strings, followed by the variable');
    end
end


% 1/30/17 integrated vargin for posinterp
currDir = pwd;
cd(rawDir);
epochTimes= getEpochs(1);
dayString = sprintf('%02i',sessionNum);

%%%%%%%%%%%%%%%%%%%
% create all rawpos per epoch
%%%%%%%%%%%%%%%%%%%

cd(dataDir);
if isempty(dir([animID 'rawpos' dayString '*-*.mat']));
    cd(rawDir);
    disp('processing individual rawpos');
    if ~isempty(dioFramePulseChannelName)
        rn_createNQRawPosFiles(dataDir,animID,sessionNum, dioFramePulseChannelName);
    else
        rn_createNQRawPosFiles(dataDir,animID,sessionNum);
    end
else
    disp('individual rawpos already processed');
end

%%%%%%%%%%%%%%%%%%%%%%%%%
% next, create one rawpos for day
%%%%%%%%%%%%%%%%%%%%%%%%%

cd(dataDir);
if isempty(dir([animID 'rawpos' dayString '.mat']));
    disp('processing rawpos');
    rawposFiles=dir([animID 'rawpos' dayString '*-*.mat']);
    rawpos={};

    for e=1:length(rawposFiles);
        temp= load(rawposFiles(e).name);

        data= [temp.rawpos{sessionNum}{e}.time,...
               temp.rawpos{sessionNum}{e}.x1, temp.rawpos{sessionNum}{e}.y1...
               temp.rawpos{sessionNum}{e}.x2, temp.rawpos{sessionNum}{e}.y2];

        rawpos{sessionNum}{e}.data    = data;
        rawpos{sessionNum}{e}.fields  = 'timestamp x1 y1 x2 y2';
        rawpos{sessionNum}{e}.units   = temp.rawpos{sessionNum}{e}.units;
        rawpos{sessionNum}{e}.cmperpixel = temp.rawpos{sessionNum}{e}.cmperpixel;

        startStr= datestr(seconds(epochTimes(e,1)), 'HH:MM:SS');
        endStr= datestr(seconds(epochTimes(e,2)), 'HH:MM:SS');
        rawpos{sessionNum}{e}.descript= {['position data from ' startStr ' to ' endStr]};
    end

    save([dataDir filesep animID 'rawpos' dayString '.mat'], 'rawpos');

else
    disp('rawpos already processed');
end

%%%%%%%%%%%%%%%%%%
% next, create pos
%%%%%%%%%%%%%%%%%%

if isempty(dir([animID 'pos' dayString '.mat']));
    disp('processing pos');
    rawposFile=dir(['*rawpos' dayString '.mat']);
    load(rawposFile.name);

    epochNum= size(epochTimes,1);


    pos={};
    for e=1:epochNum;
        if ~isempty(rawpos{sessionNum}{e}.data);

            % to account for changing diodenum
            if numel(diodenum)>1
                dn = diodenum(e);
            else
                dn = diodenum;
            end

            % get cm/px and convert- no longer necessary!
            % RN EDIT 11/8/17: cm/px conversion now occurs in rn_createNQRawPosFiles if range was set during tracking
        %           if range was not set, rn_createNQRawPosFiles will prompt to calculate cmperpx
            pos{sessionNum}{e} = rawpos{sessionNum}{e};
            
            % interpolate any jumps
            
               % convert back to px for interpolation
               tmppos = pos{sessionNum}{e};
               if strcmp(tmppos.units,'cm')
                    tmppos.data(:,2:end) = tmppos.data(:,2:end)./tmppos.cmperpixel;
                    tmppos.units = 'px';
               end
            tmppos = mcz_posinterp(tmppos,...
                'diodenum', dn,'diode', diode,'maxv', maxv,...
                'maxdevpoints', maxdevpoints, 'reversex', reversex,'reversey', reversey,...
                'bounds', bounds,'pinterp',pinterp,'maxpinterp',maxpinterp,'maxdinterp',maxdinterp);
            if strcmp(tmppos.units,'px')
                tmppos.data(:,2:end) = tmppos.data(:,2:end).*tmppos.cmperpixel;
                tmppos.units='cm';
            end
            pos{sessionNum}{e} = tmppos;
            % add smoothed velocity
            pos{sessionNum}{e} = rn_addvelocity(pos{sessionNum}{e}, posfilt);

            %TODO: add loess smoothed position

        else
            pos{sessionNum}{e}.data = [];
        end

    end

    save([dataDir filesep animID 'pos' dayString '.mat'],'pos');

else
    disp('pos already processed');
end


end
