function rn_addtetrodedescription(animdirect,fileprefix,tetrodes,descrip,days,epochs)
% Similar to sj_addtetrodelocation except instead of location ($area), adds
% a description ($descrip)

% This function adds tetrode location to both cellinfo and tetinfo. If
% tetinfo is not defined, this function will add location only to cellinfo.
% Note that this assumes each tetrode was in one and only one location
% throughout the recordings.
% Roshan 7/25/17: EDITED to allow optional days and epochs arrays to be fed so
% that the tetrode & cell descrips are only edited for those days and/or
% epochs. Can pass empty array [] as days in order to apply to specific epochs
% on all days.

try
    load([animdirect, fileprefix,'cellinfo']);
    o = cellfetch(cellinfo,'numspikes');
    targetcells = o.index(find(ismember(o.index(:,3),tetrodes)),:);
    if exist('days','var') && ~isempty(days)
        targetcells = targetcells(find(ismember(targetcells(:,1),days)),:);
    end
    if exist('epochs','var') && ~isempty(epochs)
        targetcells = targetcells(find(ismember(targetcells(:,2),epochs)),:);
    end
    for i = 1:size(targetcells,1)
        cellinfo{targetcells(i,1)}{targetcells(i,2)}{targetcells(i,3)}{targetcells(i,4)}.descrip = descrip;
        save([animdirect, fileprefix,'cellinfo'], 'cellinfo')
    end
catch
    warning('cellinfo structure does not exist')
end



try
    load([animdirect, fileprefix, 'tetinfo']);
    a = cellfetch(tetinfo, 'depth');
    targettets = a.index(find(ismember(a.index(:,3),tetrodes)),:);
    if exist('days','var') && ~isempty(days)
        targettets = targettets(find(ismember(targettets(:,1),days)),:);
    end
    if exist('epochs','var') && ~isempty(epochs)
        targettets = targettets(find(ismember(targettets(:,2),epochs)),:);
    end
    for i =1:size(targettets,1)
        tetinfo{targettets(i,1)}{targettets(i,2)}{targettets(i,3)}.descrip = descrip;
    end
    save([animdirect, fileprefix,'tetinfo'], 'tetinfo');
catch
    warning('tetinfo structure does not exist')
end
