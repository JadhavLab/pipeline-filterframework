function rn_createNQLFPFiles(rawDir, dataDir,animID,sessionNum)
%createNQLFPFiles(rawDir, dataDir,animID,sessionNum)
%
%RN EDIT 8/3/17: CHANGE FROM mcz_createNQLFPFiles: fix to work with LFP data from different versions of Trodes. Some LFP dat files have ntrode_channel_1based instead of ntrode_channel
%
%This function extracts LFP information and saves data in the FF format.
%It is assumed that there is
%one (and only one) folder named '*.LFP' in the current working
%directory that contains binary files for each LFP channel (result of extractLFPBinaryFiles.m)
%
%The function also assumes that there is a '*.time' folder in the current directory
%conaining time information about the session (from
%extractTimeBinaryFile.m).
%
%
%rawDir -- the directory where the raw dat folders are located
%dataDir -- the directory where the processed files should be saved
%animID -- a string identifying the animal's id (appended to the
%beginning of the files).
%sessionNum -- the session number (in chronological order for the animal)
disp('Processing LFPs...')
currDir = pwd;
cd(rawDir);
filesInDir = dir('*.LFP');
if ~isempty(filesInDir)
    filesInDir = filesInDir([filesInDir.isdir]);
end
if ~isempty(filesInDir) && numel(filesInDir)==1
    targetFolder = filesInDir.name;
else
    targetFolder = [];
    error('LFP folder not found in this directory.');
end

epochList = getEpochs(1);  %assumes that there is at least a 1-second gap in data between epochs

cd(targetFolder);
datFiles = dir('*.LFP_*.dat');

if (isempty(datFiles))
    cd(currDir);
    error('No LFP binary files found in LFP folder.');
end

timeDatFiles = dir('*.timestamps.dat');

if (isempty(datFiles))
    cd(currDir);
    error('No timestamps file found in LFP folder.');
end
timeData = readTrodesExtractedDataFile(timeDatFiles(1).name);
timeData = double(timeData.fields(1).data) / timeData.clockrate;
if ~exist([dataDir filesep 'EEG' filesep],'dir')
    mkdir([dataDir filesep 'EEG' filesep])
end
for datFileInd = 1:length(datFiles)
    disp(datFiles(datFileInd).name);
    data = readTrodesExtractedDataFile(datFiles(datFileInd).name);

    nTrodeNum = data.ntrode_id;
    if isfield(data,'ntrode_channel')
        channelNum = data.ntrode_channel;
    elseif isfield(data,'ntrode_channel_1based')
        channelNum = data.ntrode_channel_1based;
    else
        channelNum = 0;
    end

    for e = 1:size(epochList,1)
        currentSession = sessionNum;
        currentTimeRange = epochList(e,:);
        lfpFile = sprintf('%sEEG%s%seeg%02i-%02i-%02i.mat',[dataDir filesep],filesep,animID,sessionNum,e,nTrodeNum);
        if exist(lfpFile,'file')
            fprintf('LFP already processed for %02i-%02i-%02i, continuing...\n',sessionNum,e,nTrodeNum);
            continue;
        end
        eeg = []; % mcz from Binary2FF_LFP.m
        epochDataInd = find((timeData >= currentTimeRange(1))&(timeData < currentTimeRange(2)));

        eeg{currentSession}{e}{nTrodeNum}.descript = data.description; % mcz from Binary2FF_LFP.m
        eeg{currentSession}{e}{nTrodeNum}.timerange = [timeData(epochDataInd(1)) timeData(epochDataInd(end))];
        %redundant notation
        eeg{currentSession}{e}{nTrodeNum}.clockrate = data.clockrate;  % mcz from Binary2FF_LFP.m
        eeg{currentSession}{e}{nTrodeNum}.starttime = timeData(epochDataInd(1));
        eeg{currentSession}{e}{nTrodeNum}.endtime = timeData(epochDataInd(end));
        eeg{currentSession}{e}{nTrodeNum}.samprate = data.clockrate/data.decimation;
        eeg{currentSession}{e}{nTrodeNum}.nTrode = nTrodeNum;
        eeg{currentSession}{e}{nTrodeNum}.nTrodeChannel = channelNum;
        eeg{currentSession}{e}{nTrodeNum}.data = double(data.fields(1).data(epochDataInd,1)) * data.voltage_scaling *-1;
        eeg{currentSession}{e}{nTrodeNum}.data_voltage_scaled = 1;  % mcz from Binary2FF_LFP.m
        eeg{currentSession}{e}{nTrodeNum}.data_voltage_inverted = 1;

        if strcmp(data.reference, 'on');                      % mcz from Binary2FF_LFP.m
            eeg{currentSession}{e}{nTrodeNum}.referenced = 1; % mcz from Binary2FF_LFP.m
        else                                                  % mcz from Binary2FF_LFP.m
            eeg{currentSession}{e}{nTrodeNum}.referenced = 0; % mcz from Binary2FF_LFP.m
        end                                                   % mcz from Binary2FF_LFP.m

        eeg{currentSession}{e}{nTrodeNum}.low_pass_filter = data.low_pass_filter; % mcz from Binary2FF_LFP.m
        eeg{currentSession}{e}{nTrodeNum}.voltage_scaling = data.voltage_scaling; % mcz from Binary2FF_LFP.m

        save(lfpFile,'-v6','eeg');
    end
end
cd(currDir);
