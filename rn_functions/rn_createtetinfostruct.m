function rn_createtetinfostruct(animdirect,fileprefix)
% createtetinfostruct(animdirect,fileprefix)
% createtetinfostruct(animdirect,fileprefix,append)
%
% This function creates a tetinfo file in the animal's data directory.
% For each tetrode, the depth and number of cells is saved.  If a tetinfo file
% exists and new data is being added, set APPEND to 1.

if (animdirect(end) ~= filesep)
    animdirect = [animdirect filesep];
end

tetinfo = [];
if (nargin < 3)
    append = 0;
end
if (append)
    try
        load([animdirect, fileprefix,'tetinfo']);
    end
end


%create tetinfo for all tetrodes

eegfiles = dir([animdirect, 'EEG',filesep, fileprefix, 'eeg*']);
spikefiles = dir([animdirect, fileprefix, 'spikes*']);

 for i = 1:length(eegfiles)

     load([animdirect, 'EEG',filesep, eegfiles(i).name]);
     depth =cellfetch(eeg, 'depth');

     try
       tetinfo{depth.index(1)}{depth.index(2)}{depth.index(3)}.depth = depth.values;
       tetinfo{depth.index(1)}{depth.index(2)}{depth.index(3)}.numcells = 0;
       tetinfo{depth.index(1)}{depth.index(2)}{depth.index(3)}.descrip = '';
   catch
       warning([fileprefix, 'eeg0', num2str(depth.index(1)), '-', num2str(depth.index(2)), '-', num2str(depth.index(3)), '.mat',  ' may be empty'])
   end

 end

% add number of cells on each tetrode

for i = 1:length(spikefiles)

    load([animdirect, spikefiles(i).name]);
    timerange = cellfetch(spikes, 'timerange');
    index = timerange.index;
    if isempty(index)
        continue;
    end
    tets = unique(index(:,3))';
    epochs = unique(index(:,2))';
    days = unique(index(:,1))';
    for d=days,
        for e=epochs,
            for t=tets,
                tetinfo{d}{e}{t} = sum(index(:,1)==d & index(:,2)==e & index(:,3)==t);
            end
        end
    end
end

save([animdirect,fileprefix,'tetinfo'],'tetinfo');
