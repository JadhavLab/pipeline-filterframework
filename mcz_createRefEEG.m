function [ref]= mcz_createRefEEG(rawDir, dataDir, animID, sessionNum, refData)
% This function creates referenced EEGs from EEG files ouput from
% SpikeGadgets style exportLFP, and mcz_createNQLFPFiles.m. Assumes the EEG
% data (.dat) output has NOT been referenced, assumes that the local reference
% tetrodes for each tetrode are passed in refData in a EXN matrix, where
% N= maximum number of tetrodes, and E is the number of epochs in that
% session. If a tetrode was unused, ref should be zero.

%rawDir -- the directory where the raw dat folders are located
%dataDir -- the directory where the processed files should be saved
%animID -- a string identifying the animal's id (appended to the
%beginning of the files)
%sessionNum -- the session number (in chronological order for the animal)
%refData -- an EXN matrix with the local reference for each tetrode, where
%unused tetrodes have a ref of zero.
currDir = pwd;
cd(dataDir);

if isempty(dir(['*EEG']));
    error('No EEG folder found!');
end

cd('EEG');
sString= sprintf('%02i',sessionNum);
eegref=[];

elim=size(refData,1);
tlim=size(refData,2);

for e=1:elim;
    eString= sprintf('%02i',e);
    for t=1:tlim;
        eegrefFile = sprintf('%s%sEEG%s%seegref%02i-%02i-%02i.mat',dataDir,filesep,filesep,animID,sessionNum,e,t);
        if exist(eegrefFile,'file')
            fprintf('EEG Ref file already exists for Tetrode: %02i on Day: %02i, Epoch: %02i. Continuing to next...\n',t,sessionNum,e);
            continue;
        end
        tString= sprintf('%02i',t);
        tRef= refData(e,t);
        tRefString= sprintf('%02i',tRef);

        if strcmp(tRefString,'00')  % if your tetrode is unused, don't do anything to it!
           warning(['Tetrode=' tString ' on Day=' sString ' and Epoch='...
                eString ' has not been used. Continuing to next...']);
           continue;
        end

        reference = load([animID 'eeg' sString '-' eString '-' tRefString '.mat']);
        reference = reference.eeg;

        load([animID 'eeg' sString '-' eString '-' tString '.mat']);

        if eeg{sessionNum}{e}{t}.referenced == 1;
            warning(['Tetrode=' tString ' on Day=' sString ' and Epoch='...
                eString ' has already been referenced! Continuing to next...']);
            continue
        end

        if refData(e,t) ~= t; % if your reference number is NOT the same as your number, subtract reference EEG
           if length(eeg{sessionNum}{e}{t}.data) ~= length(reference{sessionNum}{e}{tRef}.data)
               error('EEG and Ref EEG do not have the same length!');
           end
           eegref{sessionNum}{e}{t}= eeg{sessionNum}{e}{t};
           eegref{sessionNum}{e}{t}.data= eeg{sessionNum}{e}{t}.data- reference{sessionNum}{e}{tRef}.data;
           eegref{sessionNum}{e}{t}.descript = strvcat(eegref{sessionNum}{e}{t}.descript,[' wrt local Ref tet=' tRefString]);
           eegref{sessionNum}{e}{t}.referenced = 1;
        else % if your reference number is the same as your number, you are the reference tetrode. Do no subtraction, keep as is
           eegref{sessionNum}{e}{t}= eeg{sessionNum}{e}{t};
           eegref{sessionNum}{e}{t}.descript = strvcat(eegref{sessionNum}{e}{t}.descript,[' used as Ref']);
           eegref{sessionNum}{e}{t}.referenced = 1;

        end
        save(eegrefFile,'eegref');
        clear eegref eeg reference
    end
end
