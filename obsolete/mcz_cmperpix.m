function cmperpix= mcz_cmperpix(rawDir, dataDir, animID, sessionNum, epoch)

%This function generates cm/px for a video. If a first frame .jpg exists,
% it will use this frame. It is assumed that there is one (and only one) folder
%named '*time' in the current working directory that contains .dat files
%(result of extractTimeFiles.m).
%
%rawDir -- the directory where the raw dat folders are located
%dataDir -- the directory where the processed files should be saved
%animID -- a string identifying the animal's id (appended to the
%beginning of the files).
%sessionNum -- the session number (in chronological order for the animal)
%epoch -- video of epoch of interest
%
% If unix, this script will convert raw .h264 into .mp4 and generate a
% first frame .jpeg. Conversion to .mp4 must be done manually for windows
% as of 08/16/16. If an .mp4 exists on windows, this script will convert to
% .jpeg as well.



cd(rawDir);

dayString = getTwoDigitNumber(sessionNum);
epochString = getTwoDigitNumber(epoch);

[sortedFileNameMasks, ~, ~] = mcz_readTrodesTaskFile();
fileNameMask= strtok(sortedFileNameMasks{epoch},'.');

if isempty(dir([fileNameMask '*.h264']));
    error( [fileNameMask '*.h264 not found!']);
end

rawAllVids= dir([fileNameMask '*.h264']);
rawVid= rawAllVids(1).name;

%%%%%%%%%%%%%%%%%%%%
% Get/Make Video file
%%%%%%%%%%%%%%%%%%%%
if strcmp(rawDir(end),filesep)
    rawDir = rawDir(1:end-1);
end
video= [rawDir filesep fileNameMask '.mp4'];

% this block of code only executes in Linux if the .mp4 file does not
% exist. It uses ffmeg locally to convert your .h264 file to a .mp4 file.

if isempty(dir(video)) && ispc;
    error('Sorry, this can only convert videos in Linux right now, convert your video using VLC to a .mp4!');
end

if isempty(dir(video)) && isunix;
    cmd = ['ffmpeg -r 15 -i "' rawDir filesep fileNameMask '.h264" -vcodec copy "' video '" '];
    unix(cmd);
end

%%%%%%%%%%%%%%%%%%%%
% Show first frame, get distance and cm per pix
%%%%%%%%%%%%%%%%%%%%

vidImage= [rawDir fileNameMask '.fig'];

if isempty(dir(vidImage));
    disp('reading video to get first frame image');
    videoMeta= VideoReader(video);
    tempFrame= figure('Name',[animID ': Session: ' dayString ', Epoch: ' epochString],'NumberTitle','off');
    imagesc(read(videoMeta, 1));
    savefig(tempFrame, vidImage,'compact');
    close all
end

if ~isempty(dir(vidImage));
    disp('first frame found');
    firstFrame= vidImage;
    
    known=[];
    while isempty(known);
        open(firstFrame);
        keepLine = 'No';
        while(strcmp(keepLine,'No'))
            [x, y]= ginput(2);
            hold on
            line1 = plot(x,y,'g');
            keepLine = questdlg('Keep Drawn Line?','Confirm','Yes','No','Yes');
            if strcmp(keepLine,'No')
                delete(line1)
            end
        end
        known= inputdlg('What is the known length in cm of the line you drew? (hit cancel to redo)');
        close all
    end
    
    cmperpix= str2num(cell2mat(known))./ sqrt((x(1)-x(2))^2 + (y(1)-y(2))^2);
    
    close all
end
end
%%%%%%%%%%%%%%%%%
% Helper Functions
%%%%%%%%%%%%%%%%%

function numString = getTwoDigitNumber(input)

if (input < 10)
    numString = ['0',num2str(input)];
else
    numString = num2str(input);
end
end