function mcz_createNQSpikesFiles(rawDir,dataDir,animID,sessionNum)
%createNQSpikesFiles(rawDir,dataDir,animID,sessionNum)
%
%This function extracts sorted spike information from matclust files 
%and saves data in the FF format. It is assumed that there is
%one (and only one) folder named '*.matclust' in the current working
%directory that contains matclust ('matclust_*.mat') files (result of createAllMatclustFiles.m and saving clustered
%data in matclust). 
%
%The function also assumes that there is a '*.time' folder in the current directory
%conaining time information about the session (from extractTimeBinaryFile.m).
%
% It also assumes position information has already been extracted, and is located in the dataDir. 
%
%rawDir -- the directory where the raw dat folders are located
%dataDir -- the directory where the processed files should be saved
%animID -- a string identifying the animal's id (appended to the
%beginning of the files).
%sessionNum -- the session number (in chronological order for the animal)
cd(rawDir);

currDir = pwd;
filesInDir = dir;
targetFolder = [];
for i=3:length(filesInDir)
    if filesInDir(i).isdir && ~isempty(strfind(filesInDir(i).name,'.matclust'))
        targetFolder = filesInDir(i).name;
        break;
    end
end

if isempty(targetFolder)
    error('Time folder not found in this directory.');
end

epochList = getEpochs();  %assumes that there is at least a 1-second gap in data between epochs if no .trodesComments file is found
cd(targetFolder);


spikes = [];
matclustfiles = dir('matclust_*');
disp('Processing MatClust files...');

%%%%%% mcz from sj_spikedayprocess %%%%%%
if isunix
posfile = sprintf('%s\%spos%02d.mat', dataDir, animID, sessionNum);
elseif ispc
posfile = sprintf('%s/%spos%02d.mat', dataDir, animID, sessionNum);
end

if exist(posfile,'file');
    pos =[];
    load(posfile);
end
%%%%%% mcz from sj_spikedayprocess %%%%%%

for matclustfileInd = 1:length(matclustfiles)
    
    [filePath,prefixName,ext] = fileparts(matclustfiles(matclustfileInd).name);
    nt = strfind(prefixName,'nt');
    nTrodeNum = str2num(prefixName(nt(end)+2:end));
    channelString = getTwoDigitNumber(nTrodeNum);
    
        clear clustattrib clustdata
        load(matclustfiles(matclustfileInd).name);
    
        %relWaves= load(['waves_' channelString]); % mcz add 12/1/16 to get spikewidth        
 
        load('times.mat', 'ranges');
        ranges= ranges(2:end,:);
        
        % this will go into times.mat, and assign matclust epochs to proper
        % epochs
        for efix= 1:size(ranges,1);
            reEpoch(efix,:)=[efix, find((ranges(efix,1) >= epochList(:,1)) & (ranges(efix,2) <= epochList(:,2)))];
        end
        
        for efix= 1:size(epochList,1);
            fixit= find(reEpoch(:,2) == efix); 
            if isempty(fixit);
            matclustEpoch(efix)= -1;
            else
            matclustEpoch(efix)= fixit;
            end
        end
    
        
        
    for e = 1:size(epochList,1);
        epochString = getTwoDigitNumber(e);
        currentSession = sessionNum;
        sessionString = getTwoDigitNumber(sessionNum);
        currentTimeRange = epochList(e,:);
        
        %we change the start and end times to match the actual first and last data points in the recording
        
%         epochDataInd = find((timeData.timestamps >= currentTimeRange(1))&(timeData.timestamps < currentTimeRange(2)));
%         starttime = timeData.timestamps(epochDataInd(1));
%         endtime = timeData.timestamps(epochDataInd(end));
        
        if exist('pos','var') && ~isempty(pos{currentSession}{e}.data);
            
          starttime = pos{currentSession}{e}.data(1,1); 
          endtime =   pos{currentSession}{e}.data(end,1);
        else
        starttime = currentTimeRange(1,1);
        endtime = currentTimeRange(1,2);
        end
        
        if ~isempty(clustattrib.clusters)
            for clustnum = 1:length(clustattrib.clusters)
                if (~isempty(clustattrib.clusters{clustnum}))
                    if (is_cluster_defined_in_epoch(clustnum,matclustEpoch(e)+1))
                        
                        spikes{currentSession}{e}{nTrodeNum}{clustnum} = [];
                        %make sure that the cluster was defined for the current time epoch.  If not, make the cell empty.
                        %if (is_cluster_defined_in_epoch(clustnum,epoch+1))
                        timepoints = clustdata.params(clustattrib.clusters{clustnum}.index,1);
                        timepoints = timepoints(find((timepoints >= starttime) & (timepoints <= endtime)));
                        
                        %%%%%% mcz from sj_spikedayprocess %%%%%%
                        if (size(clustdata.params,2) > 4)
                            amps = clustdata.params(clustattrib.clusters{clustnum}.index,2:5);
                        else
                            amps = nan(size(clustdata.params,1),1);
                        end
                        %if (size(clustdata.params,2) > 5)%THIS IS P2T #1!! Wrong?
                        %    spikewidth = mean(clustdata.params(clustattrib.clusters{clustnum}.index,6));%THIS IS P2T #1!!Wrong?
                        %else%THIS IS P2T #1!!Wrong?
                        %    spikewidth = nan(size(clustdata.params,1),1);%THIS IS P2T #1!!Wrong?
                        %end%THIS IS P2T #1!!Wrong?
                        %
			%wavesIdx= clustattrib.clusters{clustnum}.index; % get idx of spikes in clust
			%curWaves= relWaves(:,:,wavesIdx);  % get waveforms of spikes
			%peakIdx= find(max(relWaves,[],1)); % get idx of spike peak on each channel
			%trouIdx= find(min(relWaves,[],1)); % get idx of spike trough on each channel
			%width= double(trouIdx-peakIdx)*(1/30000);    % get width in idx in each channel, convert to time in s
			%width = width(find((timepoints >= starttime) & (timepoints <= endtime)),:); % winnow spike widths by current epoch
			% TO DO: pick which channel's spike width (one with max amp?), then average across these for each spike
				  
                        amps = amps(find((timepoints >= starttime) & (timepoints <= endtime)),:);
                        ampvar = var(amps);
                        [trash, maxvar] = max(ampvar);
                        amps = amps(:,maxvar);
                        timepoints = timepoints(:);
                        if isfield (clustattrib.clusters{clustnum},'tag')
                            tag = clustattrib.clusters{clustnum}.tag;
                        else
                            tag='';
                        end
                        %%%%%% mcz from sj_spikedayprocess %%%%%%
                        
                        if ~isempty(timepoints) && ~isempty(pos{currentSession}{e}.data)      % mcz
                            [posindex] = lookup(timepoints, pos{currentSession}{e}.data(:,1));% mcz
                            spikepos= [pos{currentSession}{e}.data(posindex,2:4)];            % mcz
                            
%                             figure;
%                             plot(pos{currentSession}{e}.data(:,2), pos{currentSession}{e}.data(:,3)); hold on;
%                             scatter(spikepos(:,1),spikepos(:,2), '.k'); 
%                             name= ['day:' num2str(currentSession) 'epoch:' num2str(e) 'tetrode:' num2str(nTrodeNum) 'cell:' num2str(clustnum)];
%                             title(name);
%                             figfile= ['/home/user/Desktop/cellplots/' name]; 
%                             print('-dpng', figfile, '-r300');
%                             close all;
                            
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.data(:,1) = timepoints; % mcz from sj_spikedayprocess
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.data(:,2:4) = spikepos; % mcz from sj_spikedayprocess
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.data(:,6)   = amps;     % mcz from sj_spikedayprocess
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.data(:,7)   = posindex; % mcz from sj_spikedayprocess
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.meanrate = length(timepoints)/(endtime-starttime);
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.spikewidth = spikewidth; % mcz from sj_spikedayprocess %THIS IS P2T #1!!Wrong?
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.descript = 'spike data, with valid pos and spikes';
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.fields = 'time x y dir not_used amplitude(highest variance channel) posindex';
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.timerange = currentTimeRange;
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.tag = tag;                            
                            
                        elseif ~isempty(timepoints) && isempty(pos{currentSession}{e}.data) % no position data, or not yet processed
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.data(:,1) = timepoints; % mcz from sj_spikedayprocess
                            % spikes{currentSession}{e}{nTrodeNum}{clustnum}.data(:,2:4) = [];% spikepos; % mcz from sj_spikedayprocess
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.data(:,6)   = amps;     % mcz from sj_spikedayprocess
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.data(:,7)   = zeros(size(timepoints));%posindex; % mcz from sj_spikedayprocess
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.meanrate = length(timepoints)/(endtime-starttime);
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.spikewidth = spikewidth; % mcz from sj_spikedayprocess %THIS IS P2T #1!!Wrong?
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.descript = 'spike data, with valid spikes only';
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.fields = 'time <x> <y> <dir> <not_used> amplitude(highest variance channel) <posindex>';
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.timerange = currentTimeRange;
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.tag = tag;                            
                            
                        elseif isempty(timepoints) && ~isempty(pos{currentSession}{e}.data) % position data, but no spikes
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.data = [];
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.descript = 'no spike data, with valid pos only';
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.fields = '<time> <x> <y> <dir> <not_used> <amplitude(highest variance channel)> <posindex>';
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.timerange = currentTimeRange;
                            spikes{currentSession}{e}{nTrodeNum}{clustnum}.tag = tag;
%                             figure;
%                             plot(pos{currentSession}{e}.data(:,2), pos{currentSession}{e}.data(:,3));
%                             %scatter(spikepos(:,1),spikepos(:,2), '.k'); hold on;
%                             name= ['day:' num2str(currentSession) 'epoch:' num2str(e) 'tetrode:' num2str(nTrodeNum) 'cell:' num2str(clustnum) '-nopos'];
%                             title(name);
%                             figfile= ['/home/user/Desktop/cellplots/' name]; 
%                             print('-dpng', figfile, '-r300');
%                             close all;
                        end
                        
                    else
                        spikes{currentSession}{e}{nTrodeNum}{clustnum} = [];
                    end
                end
            end
        end
        
    end
    
end
cd(dataDir)
save([animID,'spikes',sessionString], 'spikes');
cd(currDir);





%----------------------------------------------------------
%Helper functions
%-----------------------------------------------------------


function numString = getTwoDigitNumber(input)
    
if (input < 10)
    numString = ['0',num2str(input)];
else
    numString = num2str(input);
end
    

