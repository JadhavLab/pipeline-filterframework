# Pipeline-FilterFramework #

## Overview of the library

- This codebase is to convert data from .rec files to filter framework compatible dataset

## Data Types

**When you’ve finished a recording, the following files are created**

| Data type              | File extension |
| ---------              | :------------: |
| Raw Streamed Data File | .rec |
| Video File | .h264 |
| Camera Timestamps | .videoTimeStamps |
| Camera HW Timestamps (unused) | .videoTimeStamps.cameraHWFrameCount |
| State Script output | .stateScriptLog |
| Trodes Comments | .trodesComments |

**NOTE**
.stateScriptLog and .trodesComments will not appear if you did not use State Script, or if you did not annotate your epochs as you recorded.

**The following are raw files that you generate afterward (if need be, in the case of trodesComments)**

| Data type | File extension |
| :-------: | :------------: |
| Trodes Comments | .trodesComments |
| Position Tracking | .videoPositionTrackingFile |


## Processing raw Trodes data into a workable filter framework format breaks down into two main steps.

- **Break apart the .rec file into its components in a binary form (.rec → .dat)**
- **Convert the binaries into arrays compatible with FilterFramework (.dat→.mat)**

### STEP 1: Extraction --- Converting .rec to binary

Mattias has a great summary of all the things I mentioned above, how to use them, how to format you .trodesComments file, what they’re used for, and how to go about step 1 using his code. It can be found in TrodesToMatlab/Instructions.txt. The biggest point to take away is how to deal with “resets” in your timebase (put the string time reset at the top of all subsequent .trodesComments files)

To go from .rec to .mat, there are a series of executables that all start with extract in the folder where Trodes is located. These extraction programs will load your .rec file, chop out the desired data per tetrode, and create a new folder in your raw data directory containing your desired data in binary format.

| Function name | Data type | Filename |
| :-----------: | :-------: | :-------:|
| extracttime | Timestamps | <directoryname>.time |
| extractLFP | LFP (per tetrode) | <directoryname>.LFP |
| extractspikes | Spikes (per tetode) | <directoryname>.spikes |
| extractdio | DIO (per DIO) | <directoryname>.DIO |
| extractanalog | Analog (unused/untested) | <directoryname>.analog |
| createAllMatclustFiles.m | Matclust Files | <directoryname>.matclust |

**NOTE:** The matclust directory isn’t created using an extract executable, it is created using a matlab script, createAllMatclustFiles.m

#### Extraction methods
There are a few ways of running the extraction code. They all essentially do the same thing, which is run those executables.

1. Manual method: Command Line functions or Matlab functions 
2. Matlab GUI: <insert blurb about GUI here>

**1. Manual Method**

Mattias has included a windows/linux compatible wrapper for each individual export function in TrodesToMatlab that I expect will stay stable as development continues. (named extract\*BinaryFiles.m). One bug I’ve encountered is that Mattias’ export wrappers will not put your .rec files in order correctly epoch per epoch, so use at your own risk.

How can you run it from the command line? (Linux, Cygwin, and windows terminal)
```
<path to export code> ‘-.rec’ <path to rec file> …(if stitching together multiple, repeat -.rec flag and .rec path)….   (then add other flags to modify things).
```

If you add the following code to a terminal in Linux, it will send to output to a log file.
```
‘>’ <path and name to where you want the log file saved> ‘2>&1’
```

If you need to change the configuration file, the export functions will automatically find a new configuration file in the path of your rec file. No need to use the -reconfig flag. There are also other flags you can use to mess with spike identification parameters, LFP filtering, and so on.

By default, extractspikes has usedigitalreference ON (specify your digital reference in your config file before extracting for matclust)
By default, extractLFP has usedigitalreference OFF (we do this ourselves in the EEG pipeline)

Be aware and adjust according to what you need!

**2. Matlab GUI**

You can use the TrodesExtractionGUI available on github (https://github.com/nubs01/TrodesExtractionGUI) and run RN\_TrodesExtractionGUI in Matlab to convert from .rec to .dat and create the .trodesComments files. This GUI has works with Trodes 1.5 & 1.6 as of August 2017. But as mentioned above, its best to only use this after you understand what it is doing. This works on Windows, Mac and Linux (best on Linux -- the logs print out some additional debug info when run on Linux)

**UPDATE:** August 2017: The Roshan branch of the Pipeline-FilterFramework has been updated to work with Trodes 1.5 and 1.6. The code has been made completely independent of mordorCODE, so to use this branch clone it from bitbucket and checkout the Roshan branch and then in Matlab add to path with subfolders. Use the 'preprocess\_R005.m' as a template to setup your own preprocessing script. 

### STEP 2: Creating Matclust Files

Matclust files are created by
```
/trodes/TrodesToMatlab/TrodesToNeuroQuesry/createAllMatclustFiles.m
```
This function will create matclust files for all ntrodes in one recording session. It assumes that there is one '\*.spikes' folder in the current working directory that contains the extracted spike information for all ntrodes. Use 'extractSpikeBinaryFiles.m' to create these files from the raw .rec file. 

### STEP 3: Positon tracking and Clustering

*3.1. Position tracking*
*3.2. Clustering*

I’m not going to go into clustering and position tracking here, but position tracking has its own quirks. You want to start and end your file according to your true start and end times, not the start and end of the recording. Those frame ALL your subsequent data.

(DeepLabCut users. See ryan for DLC versions of CreateNQPosFiles, CreateNQRawPosFiles, and posinterp.m ... if there's any interest, I'll create a branch.)

### STEP 4: Conversion of .dat files to .mat files

I’ve written an example dayprocess script in Pipeline-FilterFramework/ExampleCode that handles the conversion from .dat to FilterFramework .mat for all data types, including EEG processing and secondary structures like linpos. It’s really well commented right now, I hope! Just requires users to read though it. I’ll add any clarification quirks below. If something isn’t explained sufficiently let me know and I can comment it.


Quirks for each:

Position-
Generating position requires conversion of video to .mp4 right now (to get the first frame of the video to determine cm/pixel). This requires either ffmpeg, libav, or someone to do it in VLC manually. There may still be mac/pc/linux convention bugs floating around, but I think I caught them all. You can use the following command to convert all videos in a single day folder to .mp4s in linux terminal before running mcz\_createNQPosFiles:

for l in $(ls); do ffmpeg -i $l $l.mp4;
Right now, generating position requires user input for a known length for each video recording, which is looped through right now when you run the script. It also requires a set of input parameters for posinterp to deal with jumps in position and such.

 Mattias has built something into camera module that can do this by default in the newer versions, generating position values in cm from the get go! If you set this, use mcz\_createNQposfiles\_new, it will skip the known length part that you already did while position tracking.

12/09/16- strobing DIO incorporated
01/30/17 – varagin for posinterp added

LFP-
LFP only generates one trace per tetrode, and only the first channel. If you had a dead channel, you have to mess around with the extractLFP code and use a different flag. You can also adjust the specific channel to extract for LFP(if you first channel is dead) in your config file, with the flag < lfpChan="#" >.


Spikes-
The spike file is using times framed by your position file at the moment. In fact, everything is built to be framed by your position tracking times. If position tracking is not done, it will use your matclust times.mat. Furthermore, if your times.mat does not include ALL epochs (why you’d do this, I’m not sure, since it’ll cause clustering problems later on, but it was a fix I made for some of Justins’ clustering data in the past) it should correctly figure out which epoch is which. The further you get into weird edge cases, the less tested and verified to work things are.

DIO-
DIO has been sanity tested by me, but largely untested in application since none of our current code is relying on it. There may be format changes or bugs which we don’t know about. I’ve tried to match it up to the past DIO structure as much as possible. I’d exercise caution with the DIO structures.

DIO from stateScriptLog-
I’ve also built a script that will construct a DIO file from just state script logs, for people running the ECU only. Sanity tested, but untested in the wild, but being tested now by Claire and Suman and it
looks good.

EEG
I want to change the naming conventions to be less confusing with our data right now, and all data coming in for the future. All our raw data is by default now with respect to ground. Therefore, all “first step” extraction (eeg<day>-<epoch>-<tetrode>) is with respect to ground. Anything “referenced” will mean it referenced to a local reference (eegref<day>-<epoch>-<tetrode>). By default in the dayprocess script, delta and theta are relative to ground only, and gamma and ripple are relative to local reference. This can be changed.  

## Who do I talk to?

* Shantanu
* Mark
* Roshan
