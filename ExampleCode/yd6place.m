
animals={'YD6'};
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'')';
    cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) ) ||  (strcmp($area, ''Ctx'') && ($numspikes > 100) )';
    timefilter = { {'DFTFsj_getlinstate', '(($state ~= -1) & (abs($linearvel) >= 5))', 6} };    
    
    % Iterator
    % --------
    iterator = 'singlecellanal';
    
    % Filter creation
    % ----------------
    psf = createfilter('animal',animals,'epochs',runepochfilter,'cells',cellfilter,'excludetime', timefilter,'iterator', iterator);
    % psf = createfilter('animal',animals,'days',dayfilter,'epochs',epochfilter,'cells',placecellfilter,'iterator', iterator);
    
    % Set analysis function
    % ----------------------
    pmf = setfilterfunction(psf, 'DFAsj_openfieldrate_tf', {'spikes', 'linpos', 'pos'}, 'binsize', 1, 'std', 2);    
     
    % Run analysis 
    % ------------
    pmf = runfilter(pmf);  % Place Field Map
    
    % tetrode metadata
 hpc   = [6 11 12 13 14 15 22 24 26];
 hpcRef=  22;
 pfc   = [1 2 3 4 16 17 18 19 20 27 28 29 30 31 32];
 pfcRef=   1;
 ctx   = [7 8 9 10 21 23];
 ctxRef= 23;
 %riptetlist = [1,2,3,4,5,6];
 
     for i=1:length(pmf.output{1, 1});
        figure;
        imagesc(flipud(pmf.output{1, 1}(i).smoothedspikerate)); colormap(jet);
        ind= pmf.output{1, 1}(i).index;
        if any(ind(3) == hpc); areaStr= 'hpc'; elseif any(ind(3) == pfc); areaStr= 'pfc'; elseif any(ind(3) == ctx); areaStr= 'ctx'; end;
        name= ['day:' num2str(ind(1)) 'epoch:' num2str(ind(2)) 'tetrode:' num2str(ind(3)) 'cell:' num2str(ind(4)) 'area:' areaStr];
        title(name);
        figfile= ['/home/user/Desktop/heatplotsimagesc_YD6/' name];
        colorbar;
        print('-dpng', figfile, '-r300');
        close all;
    end
