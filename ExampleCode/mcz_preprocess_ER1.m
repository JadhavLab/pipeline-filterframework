%% METADATA

% have usrlocal, Src_matlab, matclust, TrodesToMatlab, and Pipeline in path

%TODO will have to iterate over rawDir via sessionNum variable with multiple days
rawDir=  '/media/user/DATA/ER1_NEW/';
dataDir= '/media/user/DATA/ER1_NEW_direct/';
animID= 'ER1';

% tetrode metadata
 hpc   = [7 8 9 11 13 14 15 21 23 24 25 26];
 hpcRef=  12;
 pfc   = [1 2 3 4 18 20 27 30];
 pfcRef=   29;
%ctx   = [7 8 9 10 21 23];
 %ctxRef= 23;
 %riptetlist = [1,2,3,4,5,6];
 
 % behavior metadata
 numDays=1;
        
%% DAY DEPENDENT- pos, lfp, spikes, dio, task, linpos
for sessionNum=1:numDays;
    
    % TO DO: add mcz_posinterp vargin to mcz_createNQPosFiles
        mcz_createNQPosFiles(rawDir, dataDir, animID, sessionNum)
       %mcz_createNQPosFiles(rawDir, dataDir, animID, sessionNum, dioFramePulseChannelName)
      %  mcz_createNQLFPFiles(rawDir, dataDir, animID, sessionNum)
   %  mcz_createNQSpikesFiles(rawDir, dataDir, animID, sessionNum)
     
   %     mcz_createNQDIOFiles(rawDir, dataDir, animID, sessionNum)
%mcz_createNQDIOFilesFromStateScriptLogs(rawDir, dataDir, animID, sessionNum)

%   createtaskstruct(dataDir,animID,[sessionNum 4; sessionNum 6; sessionNum 8; sessionNum 10; sessionNum 12; sessionNum 14; sessionNum 16], 'getcoord_wtrack');
%sj_updatetaskstruct(dataDir,animID,sessionNum,[3 5 7 9 11 13 15 17], 'sleep');
%   sj_updatetaskenv(dataDir,animID,sessionNum,[4 6 8 10 12 14 16], 'wtr1');   
%            
%         sj_lineardayprocess(dataDir,animID,sessionNum,'welldist',15)            

end
%% DAY INDEPENDENT- cellinfo, tetinfo

     createtetinfostruct(dataDir,animID);
mcz_createcellinfostruct(dataDir,animID); 

 %sj_addtetrodedescription(dir1,prefix,riptetlist,'riptet'); 
 sj_addtetrodelocation(dataDir,animID,hpc,'CA1'); 
 sj_addtetrodelocation(dataDir,animID,pfc,'PFC');
 %sj_addtetrodelocation(dataDir,animID,ctx,'Ctx');
 sj_addtetrodedescription(dataDir,animID,hpcRef,'CA1Ref');
 sj_addtetrodedescription(dataDir,animID,pfcRef,'PFCRef');
 %sj_addtetrodedescription(dataDir,animID,23,'CtxRef');
 sj_addcellinfotag2(dataDir,animID); 

 %% EEG preprocess
 
%refData -- an EXN matrix with the local reference for each tetrode, where
%unused tetrodes have a ref of zero.
 
refData(3,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(4,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(5,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(6,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(7,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(8,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(9,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(10,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(11,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(12,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(13,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(14,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(15,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(16,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
refData(17,:)=[29 29 29 29 0 0 12 12 12 0 12 0 12 12 12 0 0 29 0 29 12 0 12 12 12 12 29 0 29 29 0 0];
 
     mcz_createRefEEG(rawDir, dataDir, animID, sessionNum, refData)
  mcz_deltadayprocess(rawDir, dataDir, animID, sessionNum, 'f', 'D:\DATA_ANALYSIS\usrlocal\filtering\deltafilter.mat')
  mcz_gammadayprocess(rawDir, dataDir, animID, sessionNum, 'f', 'D:\DATA_ANALYSIS\usrlocal\filtering\gammafilter.mat')
 mcz_rippledayprocess(rawDir, dataDir, animID, sessionNum, 'f', 'D:\DATA_ANALYSIS\usrlocal\filtering\ripplefilter.mat')
  mcz_thetadayprocess(rawDir, dataDir, animID, sessionNum, 'f', 'D:\DATA_ANALYSIS\usrlocal\filtering\thetafilter.mat')
 
 