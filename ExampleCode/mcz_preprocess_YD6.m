%% METADATA
% Notes
% -----
% RY - script was tested and no longer works with current pipeline
% have usrlocal, Src_matlab, matclust, TrodesToMatlab, and Pipeline in path

%TODO will have to iterate over rawDir via sessionNum variable with multiple days
rawDir=  '/media/user/DATA/YD6/20160807/';
dataDir= '/media/user/DATA/YD6_direct/';
animID= 'YD6';

% tetrode metadata
 hpc   = [6 11 12 13 14 15 22 24 26];
 hpcRef=  22;
 pfc   = [1 2 3 4 16 17 18 19 20 27 28 29 30 31 32];
 pfcRef=   1;
 ctx   = [7 8 9 10 21 23];
 ctxRef= 23;
 %riptetlist = [1,2,3,4,5,6];
 
 % behavior metadata
 numDays=1;
        
%% DAY DEPENDENT- pos, lfp, spikes, dio, task, linpos
for sessionNum=1:numDays;
    
    % TO DO: add mcz_posinterp vargin to mcz_createNQPosFiles
        mcz_createNQPosFiles(rawDir, dataDir, animID, sessionNum)
       %mcz_createNQPosFiles(rawDir, dataDir, animID, sessionNum, dioFramePulseChannelName)
        mcz_createNQLFPFiles(rawDir, dataDir, animID, sessionNum)
     mcz_createNQSpikesFiles(rawDir, dataDir, animID, sessionNum)
     
        mcz_createNQDIOFiles(rawDir, dataDir, animID, sessionNum)
%mcz_createNQDIOFilesFromStateScriptLogs(rawDir, dataDir, animID, sessionNum)

   createtaskstruct(dataDir,animID,[sessionNum 2; sessionNum 4], 'getcoord_wtrack');
sj_updatetaskstruct(dataDir,animID,sessionNum,[1 3 5], 'sleep');
   sj_updatetaskenv(dataDir,animID,sessionNum,[2 4], 'wtr1');   
            
         sj_lineardayprocess(dataDir,animID,sessionNum,'welldist',15)            

end
%% DAY INDEPENDENT- cellinfo, tetinfo

     createtetinfostruct(dataDir,animID);
mcz_createcellinfostruct(dataDir,animID); 

 %sj_addtetrodedescription(dir1,prefix,riptetlist,'riptet'); 
 sj_addtetrodelocation(dataDir,animID,hpc,'CA1'); 
 sj_addtetrodelocation(dataDir,animID,pfc,'PFC');
 sj_addtetrodelocation(dataDir,animID,ctx,'Ctx');
 sj_addtetrodedescription(dataDir,animID,hpcRef,'CA1Ref');
 sj_addtetrodedescription(dataDir,animID,pfcRef,'PFCRef');
 sj_addtetrodedescription(dataDir,animID,23,'CtxRef');
 sj_addcellinfotag2(dataDir,animID); 

 %% EEG preprocess
 
%refData -- an EXN matrix with the local reference for each tetrode, where
%unused tetrodes have a ref of zero.
 
% refData(1,:)=[1 1 1 1 0 22 23 23 23 23 22 22 22 22 22 1 1 1 1 1 23 22 23 22 0 22 1 1 1 1 1 1];
% refData(2,:)=[1 1 1 1 0 22 23 23 23 23 22 22 22 22 22 1 1 1 1 1 23 22 23 22 0 22 1 1 1 1 1 1];
% refData(3,:)=[1 1 1 1 0 22 23 23 23 23 22 22 22 22 22 1 1 1 1 1 23 22 23 22 0 22 1 1 1 1 1 1];
% refData(4,:)=[1 1 1 1 0 22 23 23 23 23 22 22 22 22 22 1 1 1 1 1 23 22 23 22 0 22 1 1 1 1 1 1];
% refData(5,:)=[1 1 1 1 0 22 23 23 23 23 22 22 22 22 22 1 1 1 1 1 23 22 23 22 0 22 1 1 1 1 1 1];
 
     mcz_createRefEEG(rawDir, dataDir, animID, sessionNum, refData)
  mcz_deltadayprocess(rawDir, dataDir, animID, sessionNum, 'f', '/home/user/Desktop/usrlocal/filtering/deltafilter.mat')
  mcz_gammadayprocess(rawDir, dataDir, animID, sessionNum, 'f', '/home/user/Desktop/usrlocal/filtering/gammafilter.mat')
 mcz_rippledayprocess(rawDir, dataDir, animID, sessionNum, 'f', '/home/user/Desktop/usrlocal/filtering/ripplefilter.mat')
  mcz_thetadayprocess(rawDir, dataDir, animID, sessionNum, 'f', '/home/user/Desktop/usrlocal/filtering/thetafilter.mat')
 
 