%% METADATA

% have Trodes, usrlocal, Src_matlab, matclust, TrodesToMatlab, and Pipeline in path
% To extract matclust files for clustering, run createAllMatclustFiles.m (located in TrodesToMatlab)

% path and animal metadata
topRawDir=  ''; % raw directory (contains one folder for each day of an experiment ONLY)
dataDir= '';    % data directory
animID= '';     % your animals name

% tetrode metadata (which tetrode is in which area, which is reference)
 hpc   = [];
 hpcRef=   ;
 pfc   = [];
 pfcRef=   ; 
 
 % experiment metadata
 cd(topRawDir)
 rawDir=dir();
 rawDir= {rawDir(3:end).name};
 numDays= length(rawDir);
 % The above code assumes raw directory contains one folder for each day of experiment ONLY

 %% DAY DEPENDENT NON EEG- pos, lfp, spikes, dio, task, linpos
 for sessionNum=1:numDays;
     
     % CONVERT RAW DATA TO BASIC FF DATA
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     % ability to reference added 12/09/16 
     % varargin added 01/30/17
     % varargin for you to use! If you do not specify, these are the defaults 
     % that are passed in from mcz_createNQPosFiles! 
    
    % params for sj_addvelocity
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    %posfilt = gaussian(30*0.5, 60); % gaussian smoothing for velocity filter
    
    % extra params
    %%%%%%%%%%%%%%
    %dioFramePulseChannelName=[]; % default strobing channel not used

    % params for mcz_posinterp
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    %diodenum = repmat(2,1,size(epochTimes,1));    %<--- this is the default used by the script 04/25/17
    %diodenum = [# # # # #];         % number of diodes/indicators used, where vector length is number of epochs. You fill this in. # 04/25/17
    %diode = .5;                     % use average between front and back diodes
    %reversex = 0;                   % don't flip pos along x
    %reversey = 0;                   % don't flip pos along y
    %maxv= 300;                      % maximum velocity in cm/s
    %maxdevpoints= 30;
    %bounds= [0 0 ; 1000 1000];      % bounds of video
    %pinterp = 1;                    % do interpolate
    %maxpinterp = 1e100;             % allow interpolation of any number of points
    %maxdinterp = 1e100;             % allow interpolation over any distance
      
      mcz_createNQPosFiles(    [topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum, $$your_varargin_go_here$$)
     % example of how to pass in variable arguments (var-Arg-Ins)
     %mcz_createNQPosFiles(    [topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum, 'diode',diode,'maxv',maxv......etc)


    % TRANSITION TO THIS WHEN USING RANGE DURING POSITION TRACKING INSTEAD OF VIDEO+FIRST FRAME METHOD
    % mcz_createNQPosFiles_new([topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum, varargin)

     mcz_createNQLFPFiles(    [topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum)
     %mcz_createNQSpikesFiles([topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum)
     mcz_createNQDIOFiles(    [topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum)
     %mcz_createNQDIOFilesFromStateScriptLogs([topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum)
     
     % FF BEHAVIOR METADATA STRUCTURES
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     createtaskstruct(   dataDir,animID, [sessionNum #; sessionNum #], 'getcoord_wtrack');
     sj_updatetaskstruct(dataDir,animID,sessionNum,[#], 'sleep');
     sj_updatetaskenv(   dataDir,animID,sessionNum,[#], 'wtr1');
     sj_updatetaskenv(   dataDir,animID,sessionNum,[#], 'wtr2');
     
     % FF SECONDARY DATA STRUCTURES
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %sj_lineardayprocess(dataDir,animID,sessionNum,'welldist',15)
     
 end
 %% DAY INDEPENDENT- cellinfo, tetinfo
 
 % FF CELL AND TET METADATA STRUCTURES
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %createtetinfostruct(dataDir,animID);
 %mcz_createcellinfostruct(dataDir,animID);
 
 % CELL AND TET METADATA POPULATION
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %sj_addtetrodedescription(dir1,prefix,riptetlist,'riptet');
 sj_addtetrodelocation(dataDir,animID,hpc,'CA1');
 sj_addtetrodelocation(dataDir,animID,ppc,'PPC');
 sj_addtetrodedescription(dataDir,animID,Ref,'Ref');
 %sj_addtetrodedescription(dataDir,animID,pfcRef,'PFCRef');
 %sj_addcellinfotag2(dataDir,animID);

 %% EEG PRIMARY PIPELINE
 
%refData -- an EX32 matrix with the local reference for each tetrode, where
%unused tetrodes have a ref of zero. E is 1:total # of epochs
% For reference tetrode numbers, reference to itself.    
% TODO- add changes over days? 
 
% filtering/filter path
filtPath= ['\usrlocal\filtering\'];

for sessionNum=1:numDays;
    
refData(1,:)=[# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #];
refData(2,:)=[# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #];

     mcz_createRefEEG([topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum, refData);
     % EEG wrt ground only
  mcz_deltadayprocess([topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum, 'f', [filtPath 'deltafilter.mat']);
  mcz_thetadayprocess([topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum, 'f', [filtPath 'thetafilter.mat']);
     % EEG wrt ground and local reference
  mcz_gammadayprocess([topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum, 'f', [filtPath 'gammafilter.mat']);
 mcz_rippledayprocess([topRawDir rawDir{sessionNum}], dataDir, animID, sessionNum, 'f', [filtPath 'ripplefilter.mat']);
 
end 
