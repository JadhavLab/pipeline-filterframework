%% METADATA

% have usrlocal, Src_matlab, matclust, TrodesToMatlab, and Pipeline in path

% path and animal metadata
topRawDir=  'E:\SJ5';
dataDir= 'E:\SJ5_direct\';
animID= 'SJ5';

% tetrode metadata
 hpc   = [9 10 11 13 23 24 25];
 hpcRef=  10; %Use as temp ref. Ref should be 26 but need to extract ch3 for ref b/c other channels are dead.
 pfc   = [1 2 19 20 29 30 31];
 pfcRef=   1; 
%ctx   = [7 8 9 10 21 23];
 %ctxRef= 23;
 %riptetlist = [1,2,3,4,5,6];
 
 % behavior metadata
 cd topRawDir
 rawDir=dir();
 rawDir= vertcat(rawDir.names);
 numDays= length(rawDir);
 
 %% DAY DEPENDENT NON EEG- pos, lfp, spikes, dio, task, linpos
 for sessionNum=1:numDays;
     
     % CONVERT RAW DATA TO BASIC FF DATA
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     % TO DO: add mcz_posinterp vargin to mcz_createNQPosFiles
     mcz_createNQPosFiles(   rawDir{sessionNum}, dataDir, animID, sessionNum)
     % mcz_createNQPosFiles( rawDir{sessionNum}, dataDir, animID, sessionNum, dioFramePulseChannelName)
     mcz_createNQLFPFiles(   rawDir{sessionNum}, dataDir, animID, sessionNum)
     mcz_createNQSpikesFiles(rawDir{sessionNum}, dataDir, animID, sessionNum)
     mcz_createNQDIOFiles(   rawDir{sessionNum}, dataDir, animID, sessionNum)
     %mcz_createNQDIOFilesFromStateScriptLogs(rawDir{sessionNum}, dataDir, animID, sessionNum)
     
     % FF BEHAVIOR METADATA STRUCTURES
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     createtaskstruct(   dataDir,animID, [sessionNum 2; sessionNum 4], 'getcoord_wtrack');
     sj_updatetaskstruct(dataDir,animID,sessionNum,[3 5 7 9 11 13 15 17 23], 'sleep');
     sj_updatetaskenv(   dataDir,animID,sessionNum,[4 6 8 10 12 14 16], 'wtr1');
     sj_updatetaskenv(   dataDir,animID,sessionNum,[22], 'wtr2');
     
     % FF SECONDARY DATA STRUCTURES
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     sj_lineardayprocess(dataDir,animID,sessionNum,'welldist',15)
     
 end
 %% DAY INDEPENDENT- cellinfo, tetinfo
 
 % FF CELL AND TET METADATA STRUCTURES
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 createtetinfostruct(dataDir,animID);
 mcz_createcellinfostruct(dataDir,animID);
 
 % CELL AND TET METADATA POPULATION
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %sj_addtetrodedescription(dir1,prefix,riptetlist,'riptet');
 sj_addtetrodelocation(dataDir,animID,hpc,'CA1');
 sj_addtetrodelocation(dataDir,animID,pfc,'PFC');
 %sj_addtetrodelocation(dataDir,animID,ctx,'Ctx');
 sj_addtetrodedescription(dataDir,animID,hpcRef,'CA1Ref');
 sj_addtetrodedescription(dataDir,animID,pfcRef,'PFCRef');
 %sj_addtetrodedescription(dataDir,animID,23,'CtxRef');
 sj_addcellinfotag2(dataDir,animID);

 %% EEG PIPELINE
 
%refData -- an EXN matrix with the local reference for each tetrode, where
%unused tetrodes have a ref of zero. TODO- add changes over days?
 
% filtering/filter path
filtPath= ['\usrlocal\filtering\'];

for sessionNum=1:numDays;
    
refData(1,:)=[0 1 0 0 0 0 0 0 10 0 10 0 10 0 0 0 0 0 1 1 0 0 10 10 10 0 0 0 1 1 1 0];
refData(2,:)=[0 1 0 0 0 0 0 0 10 0 10 0 10 0 0 0 0 0 1 1 0 0 10 10 10 0 0 0 1 1 1 0];
refData(3,:)=[0 1 0 0 0 0 0 0 10 0 10 0 10 0 0 0 0 0 1 1 0 0 10 10 10 0 0 0 1 1 1 0];
refData(4,:)=[0 1 0 0 0 0 0 0 10 0 10 0 10 0 0 0 0 0 1 1 0 0 10 10 10 0 0 0 1 1 1 0];
refData(5,:)=[0 1 0 0 0 0 0 0 10 0 10 0 10 0 0 0 0 0 1 1 0 0 10 10 10 0 0 0 1 1 1 0];


     mcz_createRefEEG(rawDir{sessionNum}, dataDir, animID, sessionNum, refData);
  mcz_deltadayprocess(rawDir{sessionNum}, dataDir, animID, sessionNum, 'f', [filtPath 'deltafilter.mat']);
  mcz_gammadayprocess(rawDir{sessionNum}, dataDir, animID, sessionNum, 'f', [filtPath 'gammafilter.mat']);
 mcz_rippledayprocess(rawDir{sessionNum}, dataDir, animID, sessionNum, 'f', [filtPath 'ripplefilter.mat']);
  mcz_thetadayprocess(rawDir{sessionNum}, dataDir, animID, sessionNum, 'f', [filtPath 'thetafilter.mat']);
 
end 