%% preprocess RN2 (4-8-2017)

% metadata
expDir = uigetdir('Select Experiment Directory');
animID = 'RN2';
animDir = [expDir filesep animID filesep];
rawDirs = dir(animDir);
rawDirs = rawDirs([rawDirs.isdir]);
rawDirs = {rawDirs.name};
rawDirs = rawDirs(~(strcmp(rawDirs,'.') | strcmp(rawDirs,'..')));
currDir = pwd;
dataDir = [expDir filesep animID '_direct' filesep];
logFile = [dataDir animID 'preprocess.log'];

if ~exist(dataDir,'dir')
    mkdir(dataDir);
end
% Order day directories
rawDirs = ListGUI(rawDirs,'Order Day Directories:','dir');
drawnow;

%% tetrode metadata
areas = {'RE','PFC','RT','CA1'};
tetLists = {[3 4 7],[2 27 28 29 30 31 32],[13 14 17 20],...
            [10 11 12 21 22 24 25 26]}; % tetrodes corresponding to each area
nTets = max(cellfun(@max,tetLists)); % This should be the max number of tetrodes on the EIB, (32 for rats, 8 for mice as of 7/25/17)
refList = {7,[30 30 32 32],7,[10 10 10 25]}; % an entiry can also be an array if reference changes on different days, one entry per region per day, s.a. refList{area}(day)
riptetlist = {[11,12],[11,22],[],[]}; % tetrodes that had sharp wave ripples. array for each day
dioPulseFrameChannel = []; % dio channel of camera strobe pulse, empty if no strobe

daysToAnalyze = [1 2 3 4];

RL = cell(1,numel(rawDirs));

% Start log
if exist(logFile,'file')
    HEADER = 1;
else
    HEADER = 0;
end
diary(logFile)
if HEADER
    fprintf('\n\n')
end
disp(datestr(now,0))
a = sprintf('%g ',daysToAnalyze);
fprintf('Preprocessing %s\nExperiment Directory: %s\nRaw Directory: %s\nData Directory: %s\nProcessing Days %s\n\n',animID,expDir,animDir,dataDir,a);
disp('Day Order:')
disp(rawDirs')
fprintf('\n\n')
%% Extract each day
for sessionNum=daysToAnalyze,
    fprintf('PreProcessing %s Day %02i...\n',animID,sessionNum);
    dayNumStr = sprintf('%02i',sessionNum);
    dayDir = [animDir rawDirs{sessionNum}]; % directory of raw data for day
    RL{sessionNum} = zeros(1,numel(areas));
    for i=1:numel(refList),
        if numel(refList{i})==1
            RL{sessionNum}(i) = refList{i};
        elseif numel(refList{i})>=numel(daysToAnalyze)
            RL{sessionNum}(i) = refList{i}(sessionNum);
        else
        error('Each element of refList must have a single ref that is used for all days or an array with a ref specified for each day to be analyzed')
        end
    end
    rn_createNQPosFiles(dayDir,dataDir,animID,sessionNum,[],'diodenum',1)
    rn_createNQLFPFiles(dayDir,dataDir,animID,sessionNum)
    mcz_createNQDIOFiles(dayDir,dataDir,animID,sessionNum)
    % createNQDIOFilesFromStateScriptLogs(rawDir, dataDir, animID, sessionNum)
    rn_createNQSpikesFiles(dayDir,dataDir,animID,sessionNum)

    %% create task structure for task with track epochs
%     createtaskstruct(dataDir,animID, [sessionNum epoch; sessionNum epoch], 'getcoord_wtrack');
%     sj_updatetaskstruct(dataDir,animID,sessionNum,[#], 'sleep');
%     sj_updatetaskenv(   dataDir,animID,sessionNum,[#], 'wtr1');
%     sj_updatetaskenv(   dataDir,animID,sessionNum,[#], 'wtr2');

    %% Filtering and Referencing
    cd(dayDir)
    epochs = getEpochs(1);
    cd(currDir)
    nEpochs = size(epochs,1);

    % This assumes you use the same reference for all epochs in a day, if you change references within a day you will need to edit this to work for you
    refData = zeros(nEpochs,nTets);
    for i=1:numel(tetLists),
        refData(:,tetLists{i}) = RL{sessionNum}(i);
    end
    mcz_createRefEEG(dayDir, dataDir, animID, sessionNum, refData)

    % Create filtered eeg data files
    filterDir = [fileparts(which('mcz_deltadayprocess.m')) filesep 'Filters' filesep];
    fprintf('Delta Filtering LFPs...\n')
    mcz_deltadayprocess(dayDir, dataDir, animID, sessionNum, 'f', [filterDir 'deltafilter.mat'])
    fprintf('Gamma Filtering LFPs...\n')
    mcz_gammadayprocess(dayDir, dataDir, animID, sessionNum, 'f', [filterDir 'gammafilter.mat'])
    fprintf('Ripple Filtering LFPs...\n')
    mcz_rippledayprocess(dayDir, dataDir, animID, sessionNum, 'f', [filterDir 'ripplefilter.mat'])
    fprintf('Theta Filtering LFPs...\n')
    mcz_thetadayprocess(dayDir, dataDir, animID, sessionNum, 'f', [filterDir 'thetafilter.mat'])
end

% Create cell and tet info metadata structures
disp('Creating cell & tet info structures')
rn_createtetinfostruct(dataDir,animID);
mcz_createcellinfostruct(dataDir,animID);

for day=daysToAnalyze,
    for i=1:numel(areas),
        rn_addtetrodelocation(dataDir,animID,tetLists{i},areas{i},day);
        rn_addtetrodedescription(dataDir,animID,RL{day}(i),[areas{i} 'Ref'],day);
        rn_addtetrodedescription(datadir,animID,riptetlist{day},'riptet',day);
    end
end

sj_addcellinfotag2(dataDir,animID);
disp('Preprocessing complete!')
disp(datestr(now,0))
diary off
