%% preprocess R005 (8-21-2017)

%% metadata
expDir = uigetdir('Select Experiment Directory');
animID = 'R005';
animDir = [expDir filesep animID filesep];
rawDirs = dir(animDir);
rawDirs = rawDirs([rawDirs.isdir]);
rawDirs = {rawDirs.name};
rawDirs = rawDirs(~(strcmp(rawDirs,'.') | strcmp(rawDirs,'..')));
currDir = pwd;
dataDir = [expDir filesep animID '_direct' filesep];
logFile = [dataDir animID 'preprocess.log'];

if ~exist(dataDir,'dir')
    mkdir(dataDir);
end

% Order day directories
rawDirs = ListGUI(rawDirs,'Order Day Directories:','dir');
drawnow;

%% tetrode metadata
areas = {'CA1'};
tetLists = {[1 2 3 4 5 6 7 8]}; % tetrodes corresponding to each area
nTets = 8; % This should be the max number of tetrodes on the EIB, (32 for rats, 8 for mice as of 7/25/17)
refList = {8}; % an entry can also be an array if reference changes on different days, one entry per region per day, s.a. refList{area}(day)
% if I have 2 areas and 3 days this would look like refList={[1 1 1],[5 5 5]} or refList={1,5};
riptetlist = repmat({[3 4 7]},1,10); % tetrodes that had sharp wave ripples. array for each day
% riptetlist = {[3 4 7], [1 2 3], [3 4 7], [3 4], [5 6], [5], [7], [1], [1 2], [1 2 3]}; % Another way of specifying rip tets for each day
dioPulseFrameChannel = []; % dio channel of camera strobe pulse, empty if no strobe

%% Variables for Position Tracking
diodenum = 1; % Number of LEDs used in tracking (1 or 2)

%% Variables for ripple detection/extraction
min_suprathresh_duration = 0.015; % min time for ripple power to be above threshold to count as a ripple (in seconds)
nstd = 3; % ripple power threshold: number of standard deviations above baseline

% Choose days to preprocess
daysToAnalyze = [1 2 3 4 5 6 7 8 9 10];

%% Start log
if exist(logFile,'file')
    HEADER = 1;
else
    HEADER = 0;
end
diary(logFile)
if HEADER
    fprintf('\n\n')
end
disp(datestr(now,0))
a = sprintf('%g ',daysToAnalyze);
fprintf('Preprocessing %s\nExperiment Directory: %s\nRaw Directory: %s\nData Directory: %s\nProcessing Days %s\n\n',animID,expDir,animDir,dataDir,a);
disp('Day Order:')
disp(rawDirs')
fprintf('\n\n')

%% Extract each day
RL = cell(1,numel(rawDirs));
for sessionNum=daysToAnalyze,
    fprintf('PreProcessing %s Day %02i...\n',animID,sessionNum);
    dayNumStr = sprintf('%02i',sessionNum);
    dayDir = [animDir rawDirs{sessionNum}]; % directory of raw data for day
    RL{sessionNum} = zeros(1,numel(areas));
    for i=1:numel(refList),
        if numel(refList{i})==1
            RL{sessionNum}(i) = refList{i};
        elseif numel(refList{i})>=numel(daysToAnalyze)
            RL{sessionNum}(i) = refList{i}(sessionNum);
        else
            error('Each element of refList must have a single ref that is used for all days or an array with a ref specified for each day to be analyzed')
        end
    end
    
    %% Extract Binaries to Matlab
    rn_createNQPosFiles(dayDir,dataDir,animID,sessionNum,[],'diodenum',diodenum)
    rn_createNQLFPFiles(dayDir,dataDir,animID,sessionNum)
    mcz_createNQDIOFiles(dayDir,dataDir,animID,sessionNum)
    % createNQDIOFilesFromStateScriptLogs(rawDir, dataDir, animID, sessionNum)
    rn_createNQSpikesFiles(dayDir,dataDir,animID,sessionNum)

    %% create task structure for task with track epochs
%     createtaskstruct(dataDir,animID, [sessionNum epoch; sessionNum epoch], 'getcoord_wtrack');
%     sj_updatetaskstruct(dataDir,animID,sessionNum,[#], 'sleep');
%     sj_updatetaskenv(   dataDir,animID,sessionNum,[#], 'wtr1');
%     sj_updatetaskenv(   dataDir,animID,sessionNum,[#], 'wtr2');

    %% Filtering and Referencing
    cd(dayDir)
    epochs = getEpochs(1);
    cd(currDir)
    nEpochs = size(epochs,1);

    % This assumes you use the same reference for all epochs in a day, if you change references within a day you will need to edit this to work for you
    refData = zeros(nEpochs,nTets);
    for i=1:numel(tetLists),
        refData(:,tetLists{i}) = RL{sessionNum}(i);
    end
    mcz_createRefEEG(dayDir, dataDir, animID, sessionNum, refData)

    % Create filtered eeg data files
    filterDir = [fileparts(which('mcz_deltadayprocess.m')) filesep 'filters' filesep];
    fprintf('Delta Filtering LFPs...\n')
    mcz_deltadayprocess(dayDir, dataDir, animID, sessionNum, 'f', [filterDir 'deltafilter.mat']) %can add option mcz...(...,'f',...,'ref',1) if you want to filter with eegref instead of eeg (default)
    fprintf('Gamma Filtering LFPs...\n')
    mcz_gammadayprocess(dayDir, dataDir, animID, sessionNum, 'f', [filterDir 'gammafilter.mat']) % for gamma filter default is to use eegref, can pass 'ref',0 to filter eeg instead
    fprintf('Ripple Filtering LFPs...\n')
    mcz_rippledayprocess(dayDir, dataDir, animID, sessionNum, 'f', [filterDir 'ripplefilter.mat']) % for ripple filter default is to use eegref, can pass 'ref',0 to filter eeg instead
    fprintf('Theta Filtering LFPs...\n')
    mcz_thetadayprocess(dayDir, dataDir, animID, sessionNum, 'f', [filterDir 'thetafilter.mat']) % for theta filter default is to use eeg, can pass 'ref',1 to filter eegref instead

    % extract ripples
    fprintf('Detecting and Extracting Ripples...\n')
    extractripples(dataDir,animID,sessionNum,riptetlist{sessionNum},min_suprathresh_duration,nstd);

    fprintf('Day %02d complete!\n\n\n',sessionNum)
end

%% Create cell and tet info metadata structures
disp('Creating cell & tet info structures')
rn_createtetinfostruct(dataDir,animID);
mcz_createcellinfostruct(dataDir,animID);

for day=daysToAnalyze,
    for i=1:numel(areas),
        rn_addtetrodelocation(dataDir,animID,tetLists{i},areas{i},day);
        rn_addtetrodedescription(dataDir,animID,RL{day}(i),[areas{i} 'Ref'],day);
        rn_addtetrodedescription(dataDir,animID,riptetlist{day},'riptet',day);
    end
end

sj_addcellinfotag2(dataDir,animID);
disp('Preprocessing complete!')
disp(datestr(now,0))
diary off
