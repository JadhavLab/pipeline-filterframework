 
function [x]= yd6fft()
% % tetrode metadata
%  hpc   = [6 11 12 13 14 15 22 24 26];
%  hpcRef=  22;
%  pfc   = [1 2 3 4 16 17 18 19 20 27 28 29 30 31 32];
%  pfcRef=   1;
%  ctx   = [7 8 9 10 21 23];
%  ctxRef= 23;

tlist=[ 1 2 3 4 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 26 27 28 29 30 31 32];

for e=1:5;
    for t= tlist;
        
        eString= getTwoDigitNumber(e);
        tString= getTwoDigitNumber(t);
        
        load(['YD6delta01-' eString '-' tString '.mat'])
        load(['YD6eeg01-' eString '-' tString '.mat'])
        load(['YD6eegref01-' eString '-' tString '.mat'])
        load(['YD6gamma01-' eString '-' tString '.mat'])
        load(['YD6ripple01-' eString '-' tString '.mat'])
        load(['YD6theta01-' eString '-' tString '.mat'])
        
        Fs= 1500;
        
        figure;
        y = eegref{1, 1}{1, e}{1, t}.data(:,1);
        Nsamps= length(eegref{1, 1}{1, e}{1, t}.data(:,1));
        y_fft = abs(fft(y));            %Retain Magnitude
        y_fft = y_fft(1:Nsamps/2);      %Discard Half of Points
        f = Fs*(0:Nsamps/2-1)/Nsamps;   %Prepare freq data for plot   
%         y_fft=abs(fft(y,1000));
%         y_fft=y_fft(1:500);
%         f= 0:1.5:750-1;
        plot(f, y_fft); hold on;
        
        y = double(ripple{1, 1}{1, e}{1,t}.data(:,1));
        Nsamps= length(ripple{1, 1}{1, e}{1, t}.data(:,1));
        y_fft = abs(fft(y));            %Retain Magnitude
        y_fft = y_fft(1:Nsamps/2);      %Discard Half of Points
        f = Fs*(0:Nsamps/2-1)/Nsamps;   %Prepare freq data for plot   
%         y_fft=abs(fft(y,1000));
%         y_fft=y_fft(1:500);
%         f= 0:1.5:750-1;
        plot(f, y_fft,'r')

        
        y = double(theta{1, 1}{1, e}{1, t}.data(:,1));
        Nsamps= length(theta{1, 1}{1, e}{1, t}.data(:,1));
        y_fft = abs(fft(y));            %Retain Magnitude
        y_fft = y_fft(1:Nsamps/2);      %Discard Half of Points
        f = Fs*(0:Nsamps/2-1)/Nsamps;   %Prepare freq data for plot
%         y_fft=abs(fft(y,1000));
%         y_fft=y_fft(1:500);
%         f= 0:1.5:750-1;
        plot(f, y_fft,'g')

        
        y = double(gamma{1, 1}{1, e}{1, t}.data(:,1));
        Nsamps= length(gamma{1, 1}{1, e}{1, t}.data(:,1));
        y_fft = abs(fft(y));            %Retain Magnitude
        y_fft = y_fft(1:Nsamps/2);      %Discard Half of Points
        f = Fs*(0:Nsamps/2-1)/Nsamps;   %Prepare freq data for plot
%         y_fft=abs(fft(y,1000));
%         y_fft=y_fft(1:500);
%         f= 0:1.5:750-1;
        plot(f, y_fft,'c')
        
        y = double(delta{1, 1}{1, e}{1, t}.data(:,1));
        Nsamps= length(delta{1, 1}{1, e}{1, t}.data(:,1));
        y_fft = abs(fft(y));            %Retain Magnitude
        y_fft = y_fft(1:Nsamps/2);      %Discard Half of Points
        f = Fs*(0:Nsamps/2-1)/Nsamps;   %Prepare freq data for plot
%         y_fft=abs(fft(y,1000));
%         y_fft=y_fft(1:500);
%         f= 0:1.5:750-1;
        
        plot(f, y_fft,'k')
        xlim([0 750])
        xlabel('Frequency (Hz)')
        ylabel('Amplitude')
%                 if any(t == hpc); areaStr= 'hpc'; elseif any(t == pfc); areaStr= 'pfc'; elseif any(t == ctx); areaStr= 'ctx'; end;
%         name= ['epoch=' eString ' tetrode=' tString ' area=' areaStr];
%                 title(name);
        legend({'eegref','ripple','theta','gamma','delta'});
%          figfile= ['/home/user/Desktop/FFT_YD6/1kpts' name];
%         print('-dpng', figfile, '-r300');

pause
close all;
        
    end
end

%%%%%%%%%%%%%%%%%
%Helper Functions
%%%%%%%%%%%%%%%%%

function numString = getTwoDigitNumber(input)
    
if (input < 10)
    numString = ['0',num2str(input)];
else
    numString = num2str(input);
end
