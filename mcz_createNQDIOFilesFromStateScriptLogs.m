function mcz_createNQDIOFilesFromStateScriptLogs(rawDir, dataDir,animID,sessionNum)
%This function extracts sorted dio information from statescript files
%and saves data in the FF format.
% 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% THIS IS UNTESTED AS OF 12/01/2016, SINCE NO ONE HAS USED IT OR HAD USE FOR IT YET % MCZ
% THIS IS A MODFICATION OF PARSE STATE SCRIPT FILE FROM TRODES TO MATLAB 
% THIS REQUIRES a .time FILE, WHICH MEANS extracttime WAS RUN AS CURRENTLY WRITTEN
% Currently assumes 8 input and 8 output by default for all epochs (from parseStateScriptFile). 
% WILL NOT PARSE MORE DIOs AS IT IS CURRENTLY WRITTEN
% PLEASE CHECK OUTPUT OF THIS FILE AND PARSESTATESCRIPTFILE.M TO MAKE SURE IT WORKS AND IS WHAT YOU WANT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%The function also assumes that there is a '*.time' folder in the current directory
%conaining time information about the session (from extractTimeBinaryFile.m).
%
%rawDir -- the directory where the raw dat folders are located
%dataDir -- the directory where the processed files should be saved
%animID -- a string identifying the animal's id (appended to the
%beginning of the files).
%sessionNum -- the session number (in chronological order for the animal)
cd(rawDir);

currDir = pwd;
filesInDir = dir;
targetFolder = [];
for i=3:length(filesInDir)
    if filesInDir(i).isdir && ~isempty(strfind(filesInDir(i).name,'.time'))
        targetFolder = filesInDir(i).name;
        break;
    end
end

if isempty(targetFolder)
    error('Time folder not found in this directory.');
end

[epochList,offsets] = getEpochs(1);  %assumes that there is at least a 1-second gap in data between epochs if no .trodesComments file is found
[sortedNames, ~, ~] = mcz_readTrodesTaskFile();
sortedNames= strtok(sortedNames, '.');

for e = 1:size(epochList,1)
    epochString = getTwoDigitNumber(e);
    currentSession = sessionNum;
    sessionString = getTwoDigitNumber(sessionNum);
    currentTimeRange = epochList(e,:);
    
    if ~isempty(dir([sortedNames{e} '.stateScriptLog'])) && ~isempty(parseStateScriptFile([sortedNames{e} '.stateScriptLog']))

        events = parseStateScriptFile([sortedNames{e} '.stateScriptLog']);
        
        % stateScript sampling rate is in ms, get to s
        times    = ([events.time]'/1000 + offsets(e).offset);
        inStates = vertcat(events.inStateDiff);
        inNum    = size( inStates,2);
        outStates= vertcat(events.outStateDiff);
        outNum   = size(outStates,2);
        
         inDir= repmat({ 'input'},[1, inNum]);
        outDir= repmat({'output'},[1,outNum]);
           direct= [inDir outDir];
           
         inLabel= repmat({ 'Din'}, [1, inNum])';  
        outLabel= repmat({'Dout'}, [1,outNum])';
           label= [strcat( inLabel,  num2str([1:inNum]'))'...
                   strcat(outLabel, num2str([1:outNum]'))' ];
        
        % get rid of times associated with printouts
        allStates     = [inStates outStates];
        allStatesIdx  = any(allStates,2);
        allStatesValid= allStates(allStatesIdx,:);
        allStatesTimes= times(allStatesIdx,:);
        
        for n=1:size(allStates,2);
            % get relevant state changes for this particular dio
            dioRel  = allStatesValid(:,n);
            dioIdx  = any(dioRel,2);
            dioState= dioRel(dioIdx);
            dioState(dioState==-1)=0;
            dioTime = allStatesTimes(dioIdx);
            
            DIO{sessionNum}{e}{n}.time= dioTime;
            DIO{sessionNum}{e}{n}.state= int8(dioState);
            DIO{sessionNum}{e}{n}.direction= direct{n};
            DIO{sessionNum}{e}{n}.id= label{n};
        end
        
    else
         inNum=8; outNum=8;
         inDir= repmat({ 'input'},[1, inNum]);
        outDir= repmat({'output'},[1,outNum]);
           direct= [inDir outDir];
           
         inLabel= repmat({ 'Din'}, [1, inNum])';  
        outLabel= repmat({'Dout'}, [1,outNum])';
           label= [strcat( inLabel,  num2str([1:inNum]'))'...
                   strcat(outLabel, num2str([1:outNum]'))' ];
        
        for n=1:(inNum + outNum);
            DIO{sessionNum}{e}{n}.time= [];
            DIO{sessionNum}{e}{n}.state= [];
            DIO{sessionNum}{e}{n}.direction= direct{n};
            DIO{sessionNum}{e}{n}.id= label{n};
        end
        
    end




end

cd(dataDir)
save([animID,'DIO',sessionString], 'DIO');
cd(currDir);
end

%----------------------------------------------------------
%Helper functions
%-----------------------------------------------------------


function numString = getTwoDigitNumber(input)
    
if (input < 10)
    numString = ['0',num2str(input)];
else
    numString = num2str(input);
end
end
