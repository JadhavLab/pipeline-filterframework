function numcells = findnumcells(datadir,animID,day)
% finds and returns the number of cells on each tetrode for animal animID on a
% given day. datadir should point to the animal direct folder containing the
% spikes file, which will be used to counts the number of cells.
numcells = [];
if datadir(end)~=filesep
    datadir = [datadir filesep];
end

spikeFile = sprintf('%s%sspikes%02d.mat',datadir,animID,day);
spikes = load(spikeFile);
spikes=spikes.spikes;

allCell = cellfetch(spikes,'meanrate');
if isempty(allCell.index)
    return;
end
di = find(allCell.index(:,1)==day);
index = allCell.index(di,:);
tets = unique(index(:,3))';
epochs = unique(index(:,2))';
numcells = zeros(1,max(tets));

for t=tets,
    for e=epochs,
        n = sum((index(:,2)==e & index(:,3)==t));
        if numcells(t)<n
            numcells(t) = n;
        end
    end
end

% Code to do the same thing using tetinfo
% PROBLEMS: - extractripples which uses this function will usually be called
%           before createtetinfo during extraction.
%           - Also, ERROR: tetinfo computes numcells incorrectly, it seems to
%           have counted the size of the cell array in
%           spikes{day}{epoch}{tetrode} without taking into account for empty
%           cells (such as when cluster 2 is used in epoch 2 but not in epoch 1
%           or vice versa), therefore it overcounts cells.
%{
tetFile = sprintf('%s%stetinfo.mat',datadir,animID);
tetinfo = load(tetFile);
tetinfo = tetinfo.tetinfo;

dat = cellfetch(tetinfo,'numcells');
di = find(dat.index(:,1)==day);

numcells = zeros(1,max(dat.index(di,3)));
indices = dat.index(di,:);
vals = [dat.values{di}];

for i=1:size(indices,1),
    if vals(i)>numcells(indices(i,3))
        numcells(indices(i,3))=vals(i);
    end
end
%}
